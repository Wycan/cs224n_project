"""
Converts questions to character level models
"""
import numbers
from rnn_util import CONSTS
import rnn_util as util
from collections import Counter

def split_nums_to_chars(input_file, output_file):
    """ Converts numbers to individual chars in @data_file. """
    with open(input_file, "r") as f_in, open(output_file, "w") as f_out:
        for i, example in enumerate(f_in):
            tokens = []
            for x in example.split():
                if is_number(x):
                    x = strip_trailing_zeros(x)
                    tokens.extend(x)
                    tokens.append(CONSTS.NUM_END_TOKEN)
                else:
                    tokens.append(x)
            f_out.write(" ".join(tokens) + "\n")

            if i != 0 and i % 100 == 0:
                print "Processed {} examples".format(i+1)
    print "Finished. Processed {} examples".format(i+1)

def concat_nums_to_tokens(text):
    """ Reverses split_nums_to_chars function above. i.e. converts split
    numbers in text into a single number. """
    tokens, num = [], []
    for x in text.split():
        if x == CONSTS.NUM_END_TOKEN:
            num = util.round_dp("".join(num))
            tokens.append(num)
            num = []
        elif is_number(x) or len(num):
            if len(tokens) and tokens[-1] == "-":
                num.append(tokens.pop())
            num.append(x)
        else:
            tokens.append(x)
    return " ".join(tokens)

def is_number(x):
    """ Returns True if the string @x is a number; false otherwise. """
    if x == "inf": return False
    try:
        float(x)
        return True
    except:
        return False

def strip_trailing_zeros(x):
    """ Removes trailing zeros from a number in string format. """
    return x.rstrip('0').rstrip('.') if '.' in x else x

def test_strip_trailing_zeros():
    x = "2.03200"
    y = "2.032"
    print x
    print y
    assert strip_trailing_zeros(x) == y

    x = "2.00"
    y = "2"
    print x
    print y
    assert strip_trailing_zeros(x) == y

    x = "2."
    y = "2"
    print x
    print y
    assert strip_trailing_zeros(x) == y

    x = "200"
    y = "200"
    print x
    print y
    assert strip_trailing_zeros(x) == y

def extract_vocab(text_file, vocab_file, sortKeys=False):
    vocab = Counter()
    with open(text_file, "r") as f:
        for line in f:
            for x in line.split():
                vocab[x] += 1
    with open(vocab_file, "w") as f:
        if sortKeys:
            for x in sorted(vocab):
                f.write("{}\t{}\n".format(x, vocab[x]))
        else:
            for x, count in vocab.most_common():
                f.write("{}\t{}\n".format(x, count))

def test_char_conversion():
    with open("test_chars.text", "r") as f_in, open("../data/test/test.text", "r") as f_tgt:
        for i, (x_hat, x) in enumerate(zip(f_in, f_tgt)):
            # if i in [3358]: continue
            try:
                assert concat_nums_to_tokens(x_hat).split() == x.split()
            except:
                print "E:\t", x
                print "E*:\t", concat_nums_to_tokens(x_hat)
                raise Exception()
            if i != 0 and i % 100 == 0:
                print "Processed {} examples".format(i+1)
        print "Finished. Processed {} examples".format(i+1)


if __name__ == "__main__":
    # test_strip_trailing_zeros()
    # split_nums_to_chars("../data/train/train.eqns", "train_chars.eqns")
    # split_nums_to_chars("../data/train/train.text", "train_chars.text")
    # split_nums_to_chars("../data/dev/dev.eqns", "dev_chars.eqns")
    # split_nums_to_chars("../data/dev/dev.text", "dev_chars.text")
    # split_nums_to_chars("../data/test/test.eqns", "test_chars.eqns")
    # split_nums_to_chars("../data/test/test.text", "test_chars.text")
    # test_char_conversion()
    # extract_vocab("../data/train/train_chars.eqns", "train_chars_eqns_vocab.txt")
    # extract_vocab("../data/train/train_chars.text", "train_chars_text_vocab.txt")
    split_nums_to_chars("../data/train/train_aug.text", "train_aug_chars.text")
    split_nums_to_chars("../data/train/train_aug.eqns", "train_aug_chars.eqns")
