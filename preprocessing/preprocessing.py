"""
Performs pre-processing of examples
"""

import rnn_util as util
from rnn_util import CONSTS
from sympy.parsing.sympy_parser import parse_expr
from sympy import srepr, sympify, Symbol, Number, Integer, Float, evaluate, solve
from collections import Counter
import numbers
import re
from char_extractor import is_number, concat_nums_to_tokens

class Preprocessor(object):
    def __init__(self):
        pass

    def generate_dataset(self, data,
                         text_file="text.txt",
                         eqns_file="eqns.txt",
                         ans_file="ans.txt",
                         text_vocab_file="text_vocab.txt",
                         eqns_vocab_file="eqns_vocab.txt",
                         log_file="log.txt"):
        """ Generates a dataset of text and equations """
        X, text_vocab, eqn_vocab = self.tokenize_data(data)
        self.n_examples = len(X)
        self.text_vocab_size = len(text_vocab)
        self.eqn_vocab_size = len(eqn_vocab)
        self.output_log(log_file)

        with open(text_file, "w") as f:
            for x in X:
                f.write(" ".join(x["text"]) + "\n")

        with open(eqns_file, "w") as f:
            for x in X:
                f.write(" ".join(x["eqns"]) + "\n")

        with open(ans_file, "w") as f:
            for x in X:
                ans = " or ".join(" ".join(map(util.round_dp, a)) for a in x["ans"])
                f.write(str(x["id"]) + "\t" + ans + "\n")

        with open(eqns_vocab_file, "w") as f:
            for token, count in eqn_vocab.most_common():
                f.write("{}\t{}\n".format(token, count))

        with open(text_vocab_file, "w") as f:
            for token, count in text_vocab.most_common():
                f.write("{}\t{}\n".format(token, count))

    def tokenize_data(self, data):
        """ Processes data. """
        # Stats on processing
        self.n_eqn_parse_fail = 0
        self.n_relabel_unknowns_fail = 0
        self.n_relabel_variables_fail = 0
        self.n_ans_parse_fail = 0
        self.n_ans_fail = 0
        self.n_solve_fail = 0

        # Initialize data containers
        X = []
        text_vocab = Counter()
        eqn_vocab = Counter()

        for t, x in enumerate(data):
            # Print progress
            if t != 0 and t % 100 == 0:
                print "Processed {} out of {}".format(t, len(data))

            # Initialize container to store example
            ex = {"id": t}

            # Check if it has an equation field
            if x[u"equations"] == "":
                self.n_eqn_parse_fail += 1
                print "Example {} skipped. No equations.".format(t)
                print x
                continue

            # Parse text [TBU: All tokens converted to strings]
            ex["text"] = util.parse_question_text(x[u"text"])
            ex["text"]  = [util.round_dp(i) if isinstance(i, numbers.Number)
                           else i for i in ex["text"]]

            # Parse equations
            ex["unkns"], ex["eqns"] = util.parse_unkns_equations(x[u"equations"])
            if ex["unkns"] is None or ex["eqns"] is None:
                self.n_eqn_parse_fail += 1
                print "Example {} skipped. Cannot parse equations.".format(t)
                print x
                continue

            # Relabel unknowns
            try:
                ex["unkns"], ex["eqns"] = self.add_unkn_equations(ex["unkns"], ex["eqns"])
            except:
                self.n_relabel_unknowns_fail += 1
                print "Example {} skipped. Failed to relabel unknowns.".format(t)
                print x
                continue

            # Relabel variables
            try:
                ex["unkns"], ex["eqns"] = self.relabel_variables(ex["unkns"], ex["eqns"])
            except:
                self.n_relabel_variables_fail += 1
                print "Example {} skipped. Failed to relabel variables.".format(t)
                print x
                continue

            # Parse answer
            ex["ans"] = util.parse_answer(x[u"ans"])
            if ex["ans"] is None:
                self.n_ans_parse_fail += 1
                print "Example {} skipped. Cannot parse answer.".format(t)
                print x
                continue

            # Solve equations
            solns = None
            try:
                with util.time_limit(1):
                    solns = self.solve_equations(ex["unkns"], ex["eqns"])
            except:
                self.n_solve_fail += 1
                print "Example {} skipped. Cannot solve equations.".format(t)
                print x
                print "Parsed equations", ex["eqns"]
                print "Parsed unknowns", ex["unkns"]
                continue

            # Check correct
            if solns is not None and not util.is_correct(solns, ex["ans"]):
                self.n_ans_fail += 1
                print "Example {} skipped. Answer incorrect.".format(t)
                print x
                continue

            # Tokenize equations
            ex["eqns"] = self.tokenize_equations(ex["eqns"])
            X.append(ex)

            # Update vocab counters
            for token in ex["text"]:
                if not token: continue
                text_vocab[token] += 1
            for token in ex["eqns"]:
                if not token: continue
                eqn_vocab[token] += 1

        print "Total: {} out of {} successfully processed.".format(len(X), len(data))
        print "- Failed to parse equation: {}".format(self.n_eqn_parse_fail)
        print "- Failed to relabel unknowns: {}".format(self.n_relabel_unknowns_fail)
        print "- Failed to relabel variables: {}".format(self.n_relabel_variables_fail)
        print "- Failed to parse answer: {}".format(self.n_ans_parse_fail)
        print "- Failed to solve: {}".format(self.n_solve_fail)
        print "- Failed to get correct solution: {}".format(self.n_ans_fail)
        return X, text_vocab, eqn_vocab

    def add_unkn_equations(self, unkns, eqns):
        """ Add equations for unknowns and relabel unknowns. """
        # Add equations for runknowns
        new_unkns, new_eqns = [], eqns
        for i,u in enumerate(unkns):
            new_unkns.append(Symbol("u_{}".format(i)))
            if len(u.args) != 0:
                unkn_eqn = util.parse_equation(str(new_unkns[-1]) + "=" + str(u))
                new_eqns.append(unkn_eqn)

        # Perform unknown relabellings
        unkn_sub_dict = {u: new_unkns[i] for i, u in enumerate(unkns) if len(u.args) == 0}
        with evaluate(False):
            new_eqns = [eqn.subs(unkn_sub_dict) for eqn in new_eqns]
        return new_unkns, new_eqns

    def relabel_variables(self, unkns, eqns):
        """ Relabels variables. """
        var_atoms = set(i for eqn in eqns for i in eqn.atoms(Symbol) if i not in unkns)
        var_sub_dict = {k: Symbol("x_{}".format(i)) for i,k in enumerate(var_atoms)}
        with evaluate(False):
            eqns = [eqn.subs(var_sub_dict) for eqn in eqns]
        return unkns, eqns

    def solve_equations(self, unkns, eqns):
        """ Solves equations. """
        solns = []
        with evaluate(True):
            raw_soln = solve(eqns, minimal=True, check=False)
        if type(raw_soln) == dict:
            raw_soln = [raw_soln]
        for s in raw_soln:
            soln = {u: float(u.subs(s).evalf()) for u in unkns}
            solns.append(soln)
        return solns

    def tokenize_equations(self, eqns):
        """ Tokenizes a system of equations. """
        eqn_tokens = reduce(lambda x,y: x + [CONSTS.EQN_END_TOKEN] + y,
                            [util.tokenize_eqn(eqn) for eqn in eqns])
        return eqn_tokens + [CONSTS.EQN_END_TOKEN]

    def merge_vocab(self, text_vocab_file, eqn_vocab_file, merged_vocab_file, min_frequency):
        """ Outputs a truncated vocab file for both text and eqns. """
        with open(merged_vocab_file, "w") as merged_f:
            vocab = set()
            with open(eqn_vocab_file, "r") as eqn_f:
                for line in eqn_f:
                    token, count = line.split("\t")
                    if int(count) < min_frequency: break
                    merged_f.write(token + "\n")
                    vocab.add(token)
            n_eqn_tokens = len(vocab)

            with open(text_vocab_file, "r") as text_f:
                for i, line in enumerate(text_f):
                    token, count = line.split("\t")
                    if int(count) < min_frequency: break
                    if token in vocab: continue
                    merged_f.write(token + "\n")
                    vocab.add(token)
            n_text_tokens = len(vocab) - n_eqn_tokens

        print "Total vocab size: {}".format(n_eqn_tokens + n_text_tokens)
        print "Eqn vocab size: {}".format(n_eqn_tokens)
        print "Text vocab size: {}".format(n_text_tokens)

    def output_log(self, log_file):
        """ Writes log of Preprocessor. """
        with open(log_file, "w") as f:
            for attr, value in self.__dict__.iteritems():
                f.write(attr + "\t" + str(value) + "\n")

    def reconstruct_unkns_and_equations(self, eqn_tokens):
        """ Recovers the unkns and system of equations in Sympy format from
        the list of @eqn_tokens (list of token strings not token_ids). """
        # Empty tokens
        if len(eqn_tokens) <= 2:
            return None, None

        # Retrieve unknowns
        unkns = filter(lambda x: x.startswith("u_"), eqn_tokens)
        unkns = [sympify(x) for x in sorted(set(unkns))]

        # Parse equation tokens
        eqns, eqn = [], []
        for i,x in enumerate(eqn_tokens[:-1]):
            if x == CONSTS.EQN_END_TOKEN:
                with evaluate(False):
                    sympy_eqn = sympify("".join(eqn))
                eqns.append(sympy_eqn)
                eqn = []
                continue
            if len(eqn) and x not in [")", "("] and eqn[-1] != "(":
                eqn.append(",")
            x = re.sub("<|>", "", x)
            eqn.append(x)
        sympy_eqn = sympify("".join(eqn))
        eqns.append(sympy_eqn)
        return unkns, eqns

    def reconstruct_unkns_and_equations_chars(self, eqn_tokens):
        """ Similar to reconstruct_unkns_and_equations but for equations
        where the numbers are in character format. """
        # Empty tokens
        if len(eqn_tokens) <= 2:
            return None, None

        # Parse equation tokens
        # print "Original", eqn_tokens
        eqn_tokens = concat_nums_to_tokens(" ".join(eqn_tokens)).split()
        # print "New", eqn_tokens
        unkns, eqns = self.reconstruct_unkns_and_equations(eqn_tokens)
        return unkns, eqns

    def load_examples(self, examples_file):
        """ Loads examples (text or eqns) from file"""
        with open(examples_file, "r") as f:
            examples = [x.split() for x in f]
        return examples

    def load_ans(self, ans_file):
        """ Loads answers from a given file. """
        ans = {}
        # ans = []
        with open(ans_file, "r") as f:
            for line in f:
                x_id, data = line.split("\t")
                ans[int(x_id)] = [map(float, x.split()) for x in data.split(" or ")]
                # ans.append([map(float, x.split()) for x in data.split(" or ")])
        return ans

def test():
    data = util.load_data("../data/eval_cleaned.json")[:15]
    preprocessor = Preprocessor()
    preprocessor.generate_dataset(data=data,
                                  text_file="text_test.txt",
                                  eqns_file="eqns_test.txt",
                                  ans_file="ans_test.txt",
                                  text_vocab_file="text_vocab_test.txt",
                                  eqns_vocab_file="eqns_vocab_test.txt",
                                  log_file="log_test.txt")
    loaded_text = preprocessor.load_examples("text_test.txt")
    loaded_eqns = preprocessor.load_examples("eqns_test.txt")
    loaded_ans = preprocessor.load_ans("ans_test.txt")
    preprocessor.merge_vocab("text_vocab_test.txt", "eqns_vocab_test.txt",
                             "merged_vocab_test.txt", min_frequency=3)

    for i, x in enumerate(data):
        print "\nExample {}".format(i)
        print x[u"text"]
        print loaded_text[i]
        print x[u"equations"]
        unkns, eqns = preprocessor.reconstruct_unkns_and_equations(loaded_eqns[i])
        print "Unkns", unkns
        print "Eqns", eqns
        solns = preprocessor.solve_equations(unkns, eqns)
        print "Solns", solns
        print "Ans", loaded_ans[i]
        assert util.is_correct(solns, loaded_ans[i]) == True

def generate_train_set():
    train_data = util.load_data("../data/eval_cleaned.json")
    preprocessor = Preprocessor()
    preprocessor.generate_dataset(data=train_data,
                                  text_file="train.text",
                                  eqns_file="train.eqns",
                                  ans_file="train.ans",
                                  text_vocab_file="train_text_vocab.txt",
                                  eqns_vocab_file="train_eqns_vocab.txt",
                                  log_file="train_log.txt")

def generate_dev_set():
    dev_data = util.load_data("../data/dev_cleaned.json")[:1864]
    preprocessor = Preprocessor()
    preprocessor.generate_dataset(data=dev_data,
                                  text_file="dev.text",
                                  eqns_file="dev.eqns",
                                  ans_file="dev.ans",
                                  text_vocab_file="dev_text_vocab.txt",
                                  eqns_vocab_file="dev_eqns_vocab.txt",
                                  log_file="dev_log.txt")

def generate_test_set():
    test_data = util.load_data("../data/dev_cleaned.json")[1864:]
    preprocessor = Preprocessor()
    preprocessor.generate_dataset(data=test_data,
                                  text_file="test.text",
                                  eqns_file="test.eqns",
                                  ans_file="test.ans",
                                  text_vocab_file="test_text_vocab.txt",
                                  eqns_vocab_file="test_eqns_vocab.txt",
                                  log_file="test_log.txt")

def generate_vocab():
    preprocessor = Preprocessor()
    preprocessor.merge_vocab("train_chars_text_vocab.txt", "train_chars_eqns_vocab.txt", "vocab_chars.text", min_frequency=2)

if __name__ == "__main__":
    # generate_train_set()
    # generate_dev_set()
    # generate_test_set()
    generate_vocab()
    # test()
