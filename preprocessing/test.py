import rnn_util as util
from rnn_util import CONSTS
from sympy.parsing.sympy_parser import parse_expr
from sympy import srepr, sympify, Symbol, Number, Integer, Float, UnevaluatedExpr, evaluate, solve, Not, Dummy, Ge
from words2num import words2num
import tensorflow as tf

ans = [[2.0, 84.0, 64.0], [-72.0, -64.0, -84.0]]
soln = [{"u_1": 19200.0, "u_0": 2400.0}]

print util.is_correct(soln, ans)
