"""
Data augmentation based on original data (not the char-model data).
"""
import re
import numbers
import random
import rnn_util as util
from preprocessing import Preprocessor
import string
from collections import defaultdict

MIN_NUMBER = -100.0
MAX_NUMBER = 100.0
MAX_PERMUTE_ATTEMPTS = 5

def is_number(x):
    """ Returns True if the string @x is a number; false otherwise. """
    if x == "inf": return False
    try:
        float(x)
        return True
    except:
        return False

def get_numbers_from_question(q_text):
    """ Significant numbers are all numbers exluding the ones not directly
    input into the equations. e.g. two numbers, three digits etc.
    Args:
        q_text: Question text (string)
    Returns:
        nums: dictionary of (num value, index in text)"""
    nums = defaultdict(list)
    tokens = q_text.split()
    i = -1
    for i, x in enumerate(tokens[:-1]):
        if is_number(x) and tokens[i+1] not in ["num", "int", "term", "digit"]:
            nums[x].append(i)
    if is_number(tokens[-1]):
        nums[tokens[-1]].append(i+1)
    return nums

def get_numbers_from_eqns(eqns_text):
    """ Significant numbers are numbers excluding -1 (as commonly used simply
    for negation).
    Args:
        eqns_text: Equations text (string)
    Returns:
        nums: dictionary of (num value, index in text)"""
    nums = defaultdict(list)
    for i, x in enumerate(eqns_text.split()):
        if is_number(x) and float(x) != -1:
            nums[x].append(i)
    return nums

def augment_dataset(text_file, eqns_file, ans_file,
                    new_text_file="new.text", new_eqns_file="new.eqns",
                    new_ans_file="new.ans", n_variants=5):
    """ Augments dataset by randomly permuting a single number in the
    question. """
    f_text = open(text_file, "r")
    f_eqns = open(eqns_file, "r")
    f_ans = open(ans_file, "r")

    f_new_text = open(new_text_file, "w")
    f_new_eqns = open(new_eqns_file, "w")
    f_new_ans = open(new_ans_file, "w")
    p = Preprocessor()

    for n, (text, eqns, ans) in enumerate(zip(f_text, f_eqns, f_ans)):
        # Skip if question text short (meaningless)
        if len(text.split()) <= 3: continue

        # Find the significant numbers in equations and text
        q_nums = get_numbers_from_question(text)
        eqn_nums = get_numbers_from_eqns(eqns)

        # Potential nums to permute are the unique numbers in question text that
        # also appear in the equations (on an absolute basis)
        nums = {k: v[0] for k,v in q_nums.iteritems() if len(v) == 1
                and (k in eqn_nums or util.round_dp(-float(k)) in eqn_nums)}

        # Question ID
        q_id, _ = ans.split("\t")

        # Store original example
        f_new_text.write(text)
        f_new_eqns.write(eqns)
        f_new_ans.write(ans)

        # Skip if no overlapping numbers
        if len(nums) == 0: continue

        for i in string.ascii_lowercase[:n_variants]:
            solved = False
            n_fail = 0
            while not solved and n_fail < MAX_PERMUTE_ATTEMPTS:
                # Change a single random number
                old_num = random.choice(nums.keys())
                neg_old_num = util.round_dp(-float(old_num))
                new_num = util.round_dp(random.random()*(MAX_NUMBER - MIN_NUMBER) + MIN_NUMBER)
                idx = nums[old_num]

                new_text = text.split()
                new_text[idx] = new_num
                new_text = " ".join(new_text)

                new_eqns = eqns.split()
                for idx in eqn_nums[old_num] + eqn_nums[neg_old_num]:
                    new_eqns[idx] = new_num
                new_eqns = " ".join(new_eqns)

                # Try solve
                try:
                    with util.time_limit(1):
                        symp_unkns, symp_eqns = p.reconstruct_unkns_and_equations(new_eqns.split())
                        solns = p.solve_equations(symp_unkns, symp_eqns)
                    solved = True
                except:
                    solns = None
                    n_fail += 1

            # Store permuted example
            if solns is not None:
                f_new_text.write(new_text + "\n")
                f_new_eqns.write(new_eqns + "\n")
                try:
                    solns = " or ".join(" ".join(map(util.round_dp, soln.values())) for soln in solns)
                except:
                    pass
                f_new_ans.write(q_id + i + "\t" + solns + "\n")

        if n != 0 and n % 10 == 0:
            print "Processed {}".format(n)

    f_text.close()
    f_eqns.close()
    f_ans.close()
    f_new_text.close()
    f_new_eqns.close()
    f_new_ans.close()

def test_augmented_data():
    preprocessor = Preprocessor()
    loaded_text = preprocessor.load_examples("new.text")
    loaded_eqns = preprocessor.load_examples("new.eqns")
    loaded_ans = preprocessor.load_ans("new.ans")

    for i, (l_text, l_eqns, l_ans) in enumerate(zip(loaded_text, loaded_eqns, loaded_ans)):
        unkns, eqns = preprocessor.reconstruct_unkns_and_equations(l_eqns)
        solns = preprocessor.solve_equations(unkns, eqns)
        assert util.is_correct(solns, l_ans) == True

        if i != 0 and i % 10 == 0:
            print i

if __name__ == "__main__":
    augment_dataset(text_file="../data/train/train.text",
                    eqns_file="../data/train/train.eqns",
                    ans_file="../data/train/train.ans",
                    new_text_file="train_aug.text",
                    new_eqns_file="train_aug.eqns",
                    new_ans_file="train_aug.ans", n_variants=4)
    # test_augmented_data()
