"""
Module to analyse the predictions made by the models.
"""
from preprocessing import Preprocessor
import rnn_util as util
from rnn_util import CONSTS

def output_equations(pred_eqns_file,
                     target_eqns_file,
                     question_text_file,
                     ans_file,
                     output_file,
                     chars=False):
    """ Creates a predictions.txt file that contains the question text,
    ground-truth equations and predicted equations. """
    f_pred_eqns = open(pred_eqns_file, "r")
    f_tgt_eqns = open(target_eqns_file, "r")
    f_q_text = open(question_text_file, "r")
    f_ans = open(ans_file, "r")
    f_out = open(output_file, "w")

    n_parse_fail=0
    n_solve_fail = 0
    n_incorrect = 0
    n_unk = 0

    p = Preprocessor()
    for i, (l_pred, l_tgt, l_q_text, l_ans) in enumerate(zip(f_pred_eqns, f_tgt_eqns, f_q_text, f_ans)):
        f_out.write("Q:\t" + l_q_text)
        f_out.write("E*:\t" + l_tgt)
        f_out.write("E:\t" + l_pred)

        x_id, data = l_ans.split("\t")
        ans = [map(float, x.split()) for x in data.split(" or ")]
        f_out.write("A*:\t" + str(ans) + "\n")

        eqn_tokens = l_pred.split()
        if CONSTS.UNKN_TOKEN.lower() in eqn_tokens:
            n_unk += 1

        try:
            if chars:
                unkns, eqns = p.reconstruct_unkns_and_equations_chars(eqn_tokens)
            else:
                unkns, eqns = p.reconstruct_unkns_and_equations(eqn_tokens)
        except:
            unkns, eqns = None, None
            n_parse_fail += 1

        if unkns is None or eqns is None:
            f_out.write("\n")
            continue

        try:
            with util.time_limit(1):
                solns = p.solve_equations(unkns, eqns)
        except:
            solns = None
            n_solve_fail += 1
        f_out.write("A:\t" + str(solns) + "\n\n")

        if solns is not None and not util.is_correct(solns, ans):
            n_incorrect += 1

        # if solns is not None and util.is_correct(solns, ans):
        #     f_out.write("Q:\t" + l_q_text)
        #     f_out.write("E*:\t" + l_tgt)
        #     f_out.write("E:\t" + l_pred)
        #     f_out.write("A*:\t" + str(ans) + "\n")
        #     f_out.write("A:\t" + str(solns) + "\n\n")

        if i != 0 and i % 100 == 0: print i

    # Generate summary
    f_out.write("SUMMARY--------------------------------------" + "\n")
    total = i+1
    n_correct = total - n_parse_fail - n_solve_fail - n_incorrect
    accuracy = 1.0*n_correct / total
    f_out.write("Total: {}\n".format(total))
    f_out.write("No. correct: {0} ({1:.4f})\n".format(n_correct, accuracy))
    f_out.write("No. correctly parsed: {}\n".format(total-n_parse_fail))
    f_out.write("No. solve failed: {}\n".format(n_solve_fail))
    f_out.write("No. with unks: {}\n".format(n_unk))

    f_pred_eqns.close()
    f_tgt_eqns.close()
    f_q_text.close()
    f_out.close()


if __name__ == "__main__":
    output_equations("../rnn_model/models/18_char_model_1024/output_test",
                     "../data/test/test.eqns", "../data/test/test.text",
                     "../data/test/test.ans", "predictions.txt", chars=True)
    # output_equations("../data/test/test.eqns",
    #                  "../data/test/test.eqns", "../data/test/test.text",
    #                  "../data/test/test.ans", "predictions.txt")
