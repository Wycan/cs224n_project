"""
General utility function for RNN
"""

import json
import re
import string
from sympy import simplify, sympify, Symbol, Number, Float, Integer, evaluate
from sympy.parsing.sympy_parser import parse_expr
from sympy.parsing.sympy_parser import standard_transformations, \
implicit_multiplication_application, convert_xor
from words2num import words2num
import numbers
import nltk
from nltk.translate.bleu_score import corpus_bleu
import signal
from contextlib import contextmanager
import numpy as np


class CONSTS(object):
    q_sub_dict = {      ur"once": ur"1",
                        ur"twice": ur"2",
                        ur"thrice": ur"3",
                        ur"\b(a|one)(-|\s)half": ur"0.5",
                        ur"\b(a|one)(-|\s)third": ur"0.3333",
                        ur"\b(a|one)(-|\s)fourth": ur"0.25",
                        ur"\b(a|one)(-|\s)quarter": ur"0.25",
                        ur"\b(a|one)(-|\s)fifth": ur"0.2",
                        ur"\b(a|one)(-|\s)sixth": ur"0.1667",
                        ur"\b(a|one)(-|\s)seventh": ur"0.1429",
                        ur"\b(a|one)(-|\s)eighth": ur"0.125",
                        ur"\b(a|one)(-|\s)ninth": ur"0.1111",
                        ur"\b(a|one)(-|\s)tenth": ur"0.1",
                        ur"(one|1)(\s\w*){0,3}number": ur"1_num",
                        ur"(two|2)(\s\w*){0,3}numbers": ur"2_num",
                        ur"(three|3)(\s\w*){0,3}numbers": ur"3_num",
                        ur"(four|4)(\s\w*){0,3}numbers": ur"4_num",
                        ur"(five|5)(\s\w*){0,3}numbers": ur"5_num",
                        ur"(one|1)(\s\w*){0,3}integer": ur"1_int",
                        ur"(two|2)(\s\w*){0,3}integers": ur"2_int",
                        ur"(three|3)(\s\w*){0,3}integers": ur"3_int",
                        ur"(four|4)(\s\w*){0,3}integers": ur"4_int",
                        ur"(five|5)(\s\w*){0,3}integers": ur"5_int",
                        ur"(one|1)(\s\w*){0,3}term": ur"1_term",
                        ur"(two|2)(\s\w*){0,3}terms": ur"2_term",
                        ur"(three|3)(\s\w*){0,3}terms": ur"3_term",
                        ur"(four|4)(\s\w*){0,3}terms": ur"4_term",
                        ur"(five|5)(\s\w*){0,3}terms": ur"5_term",
                        ur"(one|1)(\s\w*){0,3}digit": ur"1_digit",
                        ur"(two|2)(\s\w*){0,3}digits": ur"2_digit",
                        ur"(three|3)(\s\w*){0,3}digits": ur"3_digit",
                        ur"(four|4)(\s\w*){0,3}digits": ur"4_digit",
                        ur"(five|5)(\s\w*){0,3}digits": ur"5_digit",
                        # ur"\b(one|two|three|1|2|3)(\s|-)digit\S*": "",
                        ur"%": ur"/100",
                        }
    eqn_sub_dict = {    # Properly bracket sin, cos, log etc.
                        ur"([a-z]{3})(\w+)": ur"\1(\2)",

                        # Remove commas
                        ur",": ur""}

    printable = set(string.printable)

    transformations = (standard_transformations +
                       (implicit_multiplication_application,) + (convert_xor,))

    # Special tokens
    PADDING_TOKEN = 0
    START_TOKEN = "<START>"
    STOP_TOKEN = "<STOP>"
    EQN_END_TOKEN = "<EQN_END>"
    UNKN_TOKEN = "<UNK>"
    NUM_END_TOKEN = "<N_END>"

    PADDING_ID = 0
    START_TOKEN_ID = 1
    STOP_TOKEN_ID = 2
    EQN_END_TOKEN_ID = 3
    UNKN_TOKEN_ID = 4

    # Precision of numbers as tokens
    precision = 3


# Load data from JSON
def load_data(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data

def round_dp(x, precision=CONSTS.precision):
    format_str = "{0:." + str(int(precision)) + "f}"
    return format_str.format(float(x))

def remove_non_ascii(text):
    return filter(lambda x: x in CONSTS.printable, text)

# Remove punctuation from unicode data
def remove_punctuation(text):
    return re.sub(ur"[^a-zA-Z ]+", "", text)

# Remove non numbers from unicode data
def remove_non_numbers(text):
    return re.sub(ur"[^0-9.\/\-e\^*;]+", "", text)

def replace_exp(text):
    return re.sub(ur"[\^]", "**", text)

def remove_brackets(text):
    return re.sub(ur"[{}]+", "", text)

def remove_units(text):
    return re.sub(ur"[a-zA-Z]+", "", text)

def remove_non_printable(text):
    printable = set(string.printable)
    return filter(lambda x: x in printable, text)

def insert_mult(text):
    # Brackets
    text = re.sub(ur"([^ +-\/*\^=\(])(\()",ur"\1*\2", text)
    text = re.sub(ur"(\))([^ +-\/*\^=\)])",ur"\1*\2", text)

    # Variables
    text = re.sub(ur"(\d)([a-df-z])", ur"\1*\2", text)
    text = re.sub(ur"([a-df-z])(\d)", ur"\1*\2", text)
    text = re.sub(ur"([a-z])([a-z])", ur"\1*\2", text)

    return text

def tokenize_eqn(sympy_expr):
    """ Tokenizes a Sympy equation @sympy_expr based on its expression tree.
    Refer to http://docs.sympy.org/latest/tutorial/manipulation.html
    Args:
        sympy_expr: Sympy expression / equation.
    Returns:
        tokens: list containing tokens (strings) of equation.
    """
    if sympy_expr is None or sympy_expr == "":
        return []
    tokens = []
    if not sympy_expr.args:
        token = sympy_expr.evalf(CONSTS.precision)
        if isinstance(token, Number):
            token = round_dp(token)
        tokens.append(str(token))
    else:
        tokens.extend(["<" + sympy_expr.func.__name__ + ">", "("])
        for x in sympy_expr.args:
            tokens.extend(tokenize_eqn(x))
        tokens.append(")")
    return tokens

# Parse answer text
def parse_answer(ans_text):
    """ Parses the "ans" field @ans_text into a list of answers. Returns
    None if cannot be parsed.
    Args:
        ans_text: the "ans" field of a json example i.e. x[u"ans]
    Returns:
        ans: list of list of ints e.g. [soln2, soln2] where each solni is a
        valid / correct list of answers
    """
    ans_text = remove_non_ascii(ans_text)
    ans_text = re.sub("\{|\}", "", ans_text)
    ans = []
    for soln in ans_text.split("or"):
        x = soln.split("|")[0]
        x = remove_units(x)
        try:
            x = [float(sympify(i).evalf()) for i in x.split(";") if i]
        except:
            return None
        ans.append(x)
    return ans

def parse_equation(eqn_text):
    """ Parses a single equality equation and returns it in Sympy compatible
    format. Returns None if parsing fails.
    Args:
        eqn_text: equation in string format
    Returns:
        parsed_eqn: Sympy expression; or None
    """
    if "=" not in eqn_text:
        return None

    # Perform subsitutions
    eqn = multiple_replace(eqn_text, CONSTS.eqn_sub_dict)

    # Get in the form f(x) = 0
    eqn = re.sub("=", "-(", eqn) + ")"

    # Bracket fractions to ensure they are captured as a single atom
    # eqn = re.sub(ur"(\b\d\/\d+)", ur"(-(-\1))", eqn)

    if "_" in eqn_text:
        return None

    try:
        with evaluate(False):
            parsed_eqn = parse_expr(eqn, transformations=CONSTS.transformations)
        return parsed_eqn
    except:
        return None

def parse_unkns_equations(unkns_eqns_text):
    """ Parses "equations" field @unkns_eqns_text into a list of unknowns and
    equations in Sympy compatible format. Returns None, None if equations
    @unkns_eqns_text is null or equations are not of a compatible format.
    i.e. are not equality statements (=)
    Args:
        unkns_eqns_text: the "equations" field of a json example
        i.e. x[u"equations]
    Returns:
        parsed_unkns: list of Sympy expressions (one for each unknown)
        parsed_eqns: list of Sympy expressions (one for each equation)
    """
    # Check for non-null text
    if not unkns_eqns_text:
        return None, None

    # Split unknowns and equations
    unkns_eqns_text = remove_non_ascii(unkns_eqns_text)
    unkns_eqns = unkns_eqns_text.split("\r\n")
    unkns, eqns = unkns_eqns[0], unkns_eqns[1:]

    # Remove headers
    unkns = unkns.split(":")[-1].split(",")
    eqns = [eqn.split(":")[-1] for eqn in eqns]

    # Parse unknowns
    parsed_unkns = []
    for u in unkns:
        u = multiple_replace(u, CONSTS.eqn_sub_dict)
        try:
            with evaluate(False):
                u = parse_expr(u, transformations=CONSTS.transformations)
        except:
            return None, None
        parsed_unkns.append(u)

    # Parse equations
    parsed_eqns = []
    for eqn in eqns:
        eqn = parse_equation(eqn)
        if eqn is None: return None, None
        parsed_eqns.append(eqn)

    return parsed_unkns, parsed_eqns

def parse_question_text(q_text):
    """ Parses question text (@q_text) into a list of tokens. It replaces
    word numbers with actual numbers.
    Args:
        q_text: question text (string).
    Returns:
        parsed_tokens: list of tokens. Significant numbers are int / float; all other
        tokens are strings.
    """
    def is_number(tokens):
        x = " ".join(tokens)
        flag = isinstance(token2num(x), numbers.Number)
        return flag

    # Tokenize text
    q_text = remove_non_ascii(q_text)
    q_text = q_text.lower()
    q_text = multiple_replace(q_text, CONSTS.q_sub_dict)
    q_tokens = nltk.word_tokenize(q_text)

    # Concatenate consecutive number tokens
    i, tokens = 0, []
    while i < len(q_tokens):
        j = len(q_tokens)
        while j > i+1 and not is_number(q_tokens[i:j]):
            j -= 1
        tokens.append(token2num(" ".join(q_tokens[i:j])))
        i = j

    # Re-split tokens
    parsed_tokens = []
    for token in tokens:
        if isinstance(token, numbers.Number):
            parsed_tokens += [token]
        else:
            tokens = map(token2num, token.split("_"))
            tokens = [round_dp(x) if isinstance(x, numbers.Number) else x
                      for x in tokens]
            parsed_tokens += tokens

    return parsed_tokens

def is_correct(soln, ans):
    """ Returns true if @soln matches @ans.
    Args:
        soln: solution predicted / generated in the form of list of dicts as
        generated by the eval function of an Equation object e.g.
        [soln1, soln2, ..., solnN] where solni is a dict {x: 3, y: 5}
        ans: ground-truth answers in the form of list of lists e.g.
        [ans1, ans2, ..., ansN] where ansi is a list of [3, 5]
    Returns:
        True if @soln matches @ans; false otherwise.
    """
    if not soln or not ans:
        return False

    for s in soln:
        if not s: continue
        s = s.values()
        s.sort()
        for a in ans:
            diff = np.min(np.abs(np.array(s) - np.expand_dims(a, axis=1)), axis=1)
            percent_diff = [x*1.0/abs(y) if y != 0 else x for x, y in zip(diff, a)]
            if all(x < 0.1 for x in percent_diff):
                return True
    return False

def multiple_replace(text, sub_dict):
    for k, v in sub_dict.iteritems():
        text = re.sub(k, v, text)
    return text

def token2num(token):
    """ Converts the token to a number (if possible). Otherwise returns
    original token.
    """
    # Remove any commas
    token = re.sub(",", "", token)

    try:
        return float(token)
    except:
        pass
    try:
        return eval("1.0*"+token)
    except:
        pass
    try:
        return words2num(token)
    except:
        pass
    return token

def extract_placeholders(args, eqns):
    """ Returns a list of Sympy atoms from @eqns corresponding to each of the
    numbers in @args (in the order in which they appear). This assists with the
    mapping of numbers from question text to the equation template.
    Args:
        args: list of significant numbers from question text (float/int).
        eqns: list of parsed equations (Sympy expression for each equation).
    Returns:
        placeholders: list of Sympy atoms coresponding to each number in @args.
    """
    constants = set(x for eqn in eqns for x in eqn.atoms(Number))
    # print "constants", constants
    placeholders = []
    for x in args:
        for y in constants:
            if abs(abs(x) - abs(y)) < 0.01*abs(x):
                placeholders.append(x)
                break
    return placeholders

class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def compute_bleu_score(true_eqns, predicted_eqns):
    """ Computes the corpus-level bleu score for @predicted_eqns vs.
    gold @true_eqns.
    Args:
        true_eqns: [x1[u"equations], x2[u"equations], ..., xN[u"equations"]]
        predicted_eqns: [[eq1, eq2], [eq1], ..., [eq1, eq2, eq3]]
    Returns:
        bleu_score: corpus-level bleu score across all examples
    """
    list_of_references, hypotheses = [], []
    for x, x_hat in zip(true_eqns, predicted_eqns):
        if not x: continue
        try:
            _, x_eqns = parse_equation(x)
        except:
            continue

        if not x_eqns or not x_hat: continue
        x_tokens = reduce(lambda x, y: x + y, [eqn_tokenizer(str(i)) for i in x_eqns])
        list_of_references.append([x_tokens])

        if not x_hat:
            x_hat_tokens = []
        else:
            x_hat_tokens = reduce(lambda x, y: x + y,
                                  [eqn_tokenizer(str(i)) for i in x_hat])
        hypotheses.append(x_hat_tokens)

    bleu_score = corpus_bleu(list_of_references, hypotheses)
    return bleu_score

def test_load_data():
    print "\nChecking load data"
    data = load_data("data/dev_cleaned.json")
    print "Passed\n"

def test_remove_punctuation():
    print "\nChecking punctuation"
    x = u"Hello! I'm here"
    y = remove_punctuation(x)
    print x
    print y
    assert y == "Hello Im here"
    print "Passed\n"

    x = unicode(string.printable, "utf-8")
    y = remove_punctuation(x)
    print x
    print y
    assert y == unicode(string.ascii_letters+" ", "utf-8")
    print "Passed\n"

def test_remove_non_numbers():
    print "\nChecking removing non numbers"
    x = u"6; 9"
    y = remove_non_numbers(x)
    print x
    print y
    assert y == u"6;9"

    x = u"{6; 9; 6/2 or -4.932}"
    y = remove_non_numbers(x)
    print x
    print y
    assert y == u"6;9;6/2-4.932"

    print "Passed\n"

def test_multiple_replace():
    print "\nChecking multiple replace ..."
    x = u"hello there hello there I am well"
    sub_dict = {r"\bh\S*": u"good", u"there": u"bye"}
    y = multiple_replace(x, sub_dict)

    print x
    print y
    assert y == u"good bye good bye I am well"
    print "... passed."

def test_parse_answer():
    print "\nChecking parsing of answers ..."
    data = load_data("data/dev_cleaned.json")
    #SKIP_Q = {310}

    #print "Dataset"
    #for i, q in enumerate(data):
    #    if not q[u"equations"] or i in SKIP_Q: continue
    #    #print i, ")"
    #    #print q[u"ans"]
    #    parse_answer(q[u"ans"])
    #    #print ""

    # print "... passed."

    x = u"{3; 4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3, 4]]

    x = u"{0.33 or 4.0}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.33], [4.0]]

    x = u"{3 | 4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3]]

    x = u"{3/4 | 0.75}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75]]

    x = u"{0.75 | 3/4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75]]

    x = u"{3; 4 | 3.0; 4.0}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3, 4]]

    x = u"3.3e-07; 2^8"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3.3e-07, 2**8]]

    x = u"{0.75 | 3/4 or 4; 5; 3}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75], [4, 5, 3]]

    print "... passed."

def test_parse_equation():
    data = load_data("../data/dev_cleaned.json")
    print "\nChecking parsing equation ..."

    print "1) Checking data ..."
    for q in data[:10]:
        x = q[u"equations"]
        y = parse_unkns_equations(x)
        print x
        print y, "\n"
    print "... passed."

    print "2) Test cases ..."
    x = u"unkn: x,y\r\nequ: 2(2*x) = y\r\nequ: (1+y)(2+y)=10"
    print x
    unkns, eqns = parse_unkns_equations(x)
    print eqns, "\n"
    assert unkns == [Symbol('x'), Symbol('y')]
    assert eqns[0] == sympify("2*2*x-y", evaluate=False)
    assert eqns[1] == sympify("(y + 1)*(y + 2) - 10", evaluate=False)
    print "... passed."

def test_insert_mult():
    print "\nChecking insert_mult ..."

    x = u"(99+3)n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*n"

    x = u"(99+3)*n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*n"

    x = u"(99+3)+n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)+n"

    x = u"(99+3)-n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)-n"

    x = u"(99+3)/n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)/n"

    x = u"(99+3)^n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)^n"

    x = u"(99+3)(100 + 2)"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*(100 + 2)"

    x = u"(99+3) + (3 + 2)"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3) + (3 + 2)"

    x = u"n(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)"

    x = u"n*(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)"

    x = u"n+(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n+(99+3)"

    x = u"n-(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n-(99+3)"

    x = u"n/(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n/(99+3)"

    x = u"n^(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n^(99+3)"

    x = u"n(99+3)n(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)*n*(99+3)"

    x = u"5n(4 + 1) + xy + 7e-7"
    y = insert_mult(x)
    print x, y
    assert y == u"5*n*(4 + 1) + x*y + 7e-7"

    print "... passed."

def test_eqn_tokenizer():
    print "\nChecking equation tokenizer ..."
    x = u"5*n + 4 = 7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"5", u"*", u"n", u"+", u"4", u"=", u"7"]

    x = u"5.44*n + 4 = 7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"5.44", u"*", u"n", u"+", u"4", u"=", u"7"]

    x = u"sin(n) ^ 4e06 = -7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"sin", u"(", u"n", u")", u"^", u"4e06", u"=", u"-", u"7"]

    x = u"4.2e-06 < -7e-07 < 2*(log(x))"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"4.2e-06", u"<", u"-", u"7e-07", u"<", u"2", u"*", u"(", u"log", u"(", u"x", u")", u")"]

    print "... passed."

def test_is_number():
    print "\nChecking is_number ..."
    x = [u"Twenty"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"Twenty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"One", u"hundred", u"and", u"thirty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"Not", u"hundred", u"and", u"thirty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == False

    x = [u"Not", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == False

    x = [u"three", u"Not"]
    flag = is_number(x)
    print x, flag
    assert flag == False

def test_extract_nums():
    print "\nChecking number extraction ..."
    q_text = u"One number is 3 less than a second number. Twice the second number is 12 less than 5 times the first. Find the two numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [3, 2, 12, 5]

    q_text = u"The sum of two numbers is 125. Five times one number minus three times the other is 41. Find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [125, 5, 3, 41]

    q_text = u"1/3 of a number is 5 less than half of the same number. what is the number?"
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [1.0/3, 5, 1.0/2]

    q_text = u"Eighty, decreased by three times a number, is the same as five times the number, increased by eight. Find the number."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [80, 3, 5, 8]

    q_text = u"the product of two consecutive odd numbers is 143. find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [143]

    q_text = u"the product of two consecutive odd numbers is twenty three. find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [23]

    q_text = u"One hundred and eighty three, decreased by three times a number, is the same as five times the number, increased by eight. Find the number."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [183, 3, 5, 8]

    print "... passed."

def test_time_limit():
    import time

    print "\nChecking time_limit ..."
    try:
        with time_limit(5):
            time.sleep(5.1)
    except TimeoutException as e:
        print("Timed out!")

    try:
        with time_limit(5):
            time.sleep(4.9)
    except TimeoutException as e:
        print("Timed out!")

    print "... passed"

def test_compute_bleu_score():
    print "\nChecking compute bleu score ..."
    data = load_data("data/eval_cleaned.json")[:10]
    true_eqns = [x[u"equations"] for x in data]
    predicted_eqns = [parse_equation(x)[1] for x in true_eqns]

    print "True eqns", true_eqns
    print "Predicted", predicted_eqns

    y = compute_bleu_score(true_eqns, predicted_eqns)
    assert abs(y - 1.00) < 1e-3
    print "bleu score: {0: .4f}".format(y)
    print "... passed"

def test_round_dp():
    x = "2.23142"
    y = round_dp(x, precision=2)
    print x
    print y
    assert y == "2.23"

    x = "2.23142"
    y = round_dp(x, precision=3)
    print x
    print y
    assert y == "2.231"

    x = 2.23142
    y = round_dp(x, precision=3)
    print x
    print y
    assert y == "2.231"

if __name__ == "__main__":
    # test_insert_mult()
    # test_parse_equation()
    # test_load_data()
    # test_remove_punctuation()
    # test_multiple_replace()
    # test_remove_non_numbers()
    # test_parse_answer()
    # test_eqn_tokenizer()
    # test_is_number()
    # test_extract_nums()
    # test_time_limit()
    # test_compute_bleu_score()
    test_round_dp()
