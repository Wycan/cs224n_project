import util
import nltk
import string
import numpy as np
from collections import Counter, defaultdict
from equation import Equation, is_correct
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.feature_extraction.text import TfidfTransformer
from scipy.sparse import csr_matrix, vstack
import cPickle as pickle

"""
TBUS:
- Fix up args / placeholder / subsitution
- Lower case before tokenization
"""

class Config(object):
    min_token_count = 1
    remove_stop_words = False
    remove_punct = False

    tfidf = False
    tfidf_norm = None

class SimModel(object):
    """
    Simple similarity-based model
    """
    def __init__(self, config=Config()):
        self.config = config

        print "Similarity Model"
        print "Remove stop words", config.remove_stop_words
        print "Remove punctuation", config.remove_punct
        print "TF-IDF", config.tfidf
        print "TF-IDF norm", config.tfidf_norm

        # Set up tokens to ignore when tokenizing
        self.ignore_tokens = set()
        if self.config.remove_stop_words:
            self.ignore_tokens |= set(nltk.corpus.stopwords.words('english'))
        if self.config.remove_punct:
            self.ignore_tokens |= set(string.punctuation)

    def fit(self, json_data):
        """ Store examples that can be parsed correctly. Featurize question
        text and parse equations into an Equation object.
        Args:
            json_data: json data of examples of the form [x1, x2, ..., xN]
            where each xi is an example.
        """
        # Process each example: only keep ones that have been parsed correctly
        self.vocab = Counter()
        tokens, self.equations = [], []
        for i,x in enumerate(json_data):
            try:
                with util.time_limit(1):
                    eqns = Equation(x)
                    ans = util.parse_answer(x[u"ans"])
                    soln, _ = eqns.eval()
                    if is_correct(soln, ans):
                        q_tokens = self.tokenize(x[u"text"])
                        tokens.append(q_tokens)
                        self.equations.append(eqns)
                        print "Added example #:", i
            except:
                print "Failed to parse #:", i

        # Featurize remaining examples
        self.token_to_id, self.id_to_token = self.get_token_id_mappings(self.vocab)
        self.vocab_size = len(self.token_to_id) + 1
        word_ids = self.word_ids(tokens, self.token_to_id)
        features = self.featurize(word_ids)

        # Perform TF-IDF transformation
        if self.config.tfidf:
            self.tfidf = TfidfTransformer(norm=self.config.tfidf_norm,
                                          use_idf=True, smooth_idf=True,
                                          sublinear_tf=False)
            self.features = self.tfidf.fit_transform(features)
            print "TF-IDF performed"
        else:
            self.features = features

        # Bin examples based on number of args in equations
        templates = defaultdict(list)
        for i, x in enumerate(self.equations):
            templates[x.n_args] += [i]
        self.templates = {k: np.array(v) for k,v in templates.iteritems()}

        print "Total examples: {} out of {}".format(len(self.equations), len(json_data))

    def tokenize(self, question_text):
        """ Tokenize the text of a @question_text.
        Args:
            question_text: question text i.e. <json_example>[u"text"]
        Returns:
            tokens: tokenized version of @question_text
        """
        tokens = nltk.word_tokenize(question_text)
        for x in tokens:
            if x in self.ignore_tokens: continue
            self.vocab[x] += 1
        return tokens

    def get_token_id_mappings(self, vocab):
        """ Get dict mappings from token (word) to id (int) and vice versa. """
        token_to_id = {}
        id_to_token = {}

        for i, (w, c) in enumerate(vocab.most_common()):
            if c < self.config.min_token_count: break
            token_to_id[w] = i + 1

        id_to_token = {v: k for k,v in token_to_id.iteritems()}
        return token_to_id, id_to_token

    def word_ids(self, tokens, token_to_id):
        """ Convert tokens (list of words) to word ids (list of ints) based. """
        word_ids = []
        for q_tokens in tokens:
            q_ids = np.array([token_to_id[w] if w in token_to_id else 0
                              for w in q_tokens], dtype=np.int)
            word_ids.append(q_ids)
        return word_ids

    def featurize(self, word_ids):
        """ Extract feature matrix of token counts. """
        features = []
        for q_ids in word_ids:
            x = np.zeros(self.vocab_size)
            np.add.at(x, q_ids, 1)
            features.append(csr_matrix(x))
        return vstack(features)

    def predict(self, q_text):
        """ Computes the answer for a single question @qtext by finding the most
        similar training example and plugging in the values.
        Args:
            q_text: question text (string)
        Returns:
            soln: solution (float)
            eqns: equations (post number mapping) in string format (string)
            idx: index of nearest example (int)
        """
        q_tokens = nltk.word_tokenize(q_text)
        args = util.extract_nums(q_text)
        word_ids = self.word_ids([q_tokens], self.token_to_id)
        q_features = self.featurize(word_ids)

        if self.config.tfidf:
            q_features = self.tfidf.transform(q_features)

        if len(args) not in self.templates:
            print "No appropriate template found."
            return None, None, None
        temp_idxs = self.templates[len(args)]
        temp_features = self.features[temp_idxs]
        dist = pairwise_distances(temp_features, q_features, metric='cosine')

        idx = temp_idxs[np.argmin(dist)]
        soln, eqns = self.equations[idx].eval(args)
        # soln, eqns = self.equations[idx].eval()
        return soln, eqns, idx

    def evaluate(self, json_data):
        """ Evaluate performance over a set of examples @json_data.
        Args:
            json_data: json data of examples of the form [x1, x2, ..., xN]
            where each xi is an example (must have answers).
        Returns:
            acc: accuracy
            results: list of tuples of the form (soln, ans, eqns, idx), where:
                soln is the answers generated / predicted
                eqns is the equations predicted (post number mapping)
                idx is the index of the closest neighbor
                ans is the ground-truth answers
                correct is a bool flag denoting whether it is correct or not
        """
        results = []
        ans = []
        for i, x in enumerate(json_data):
            try:
                with util.time_limit(1):
                    soln, eqns, idx = self.predict(x[u"text"])
                    ans = util.parse_answer(x[u"ans"])
                    correct = is_correct(soln, ans)
                    results.append((soln, eqns, idx, ans, correct))
            except:
                results.append((None, None, None, None, False))

            if i % 10 == 0: print "{} out of {}".format(i, len(json_data))

        examples_correct = filter(lambda x: x[-1], results)
        acc = len(examples_correct)*1.0 / len(json_data)
        return acc, results

def test():
    config = Config()
    config.min_token_count = 1
    config.remove_stop_words = False
    config.remove_punct = False
    config.stem = False
    config.tfidf = False

    json_data = util.load_data("data/dev_cleaned.json")[:100]
    n_examples = len(json_data)
    print n_examples, "loaded.\n"
    model = SimModel(config)
    model.fit(json_data)

    # Check id mappings
    print "\nChecking id mappings ..."
    assert len(model.token_to_id) == len(model.id_to_token)
    for i, (k, v) in enumerate(model.token_to_id.iteritems()):
        assert model.id_to_token[model.token_to_id[k]] == k
        if i < 10:
            print v, model.token_to_id[k], k, model.id_to_token[v]
    print "... passed\n"

    # Check features
    # print "\nChecking features ..."
    # tokens = [model.tokenize(x[u"text"]) for x in json_data]
    # assert len(tokens) == n_examples
    # assert model.features.shape[0] == n_examples
    #
    # for i, q_features in enumerate(model.features):
    #     words = tokens[i]
    #     assert abs(np.linalg.norm(q_features) - 1.0) < 1e-03
    #     assert q_features[1:] == len([w for w in words if w in model.token_to_id])
    #     for word_id, count in enumerate(q_features):
    #         if count == 0: continue
    #         word = model.id_to_token[word_id]
    #         assert words.count(word) == count
    # print "... passed\n"

    # Check predict
    print "\nChecking predict ..."
    q_text = "A number is 24 less than another.  The sum of the numbers is 14.  Find the numbers."
    ans, eqn, idx = model.predict(q_text)
    print "\nQ:", q_text
    print "Closest", idx
    print "Equation", eqn
    print "Answer", ans
    # assert idx == 1
    assert is_correct(ans, [[19, -5]])

    q_text = "Seven more than eighteen times a number is 2 more than three times the number. what's the value of the number?"
    ans, eqn, idx = model.predict(q_text)
    print "\nQ:", q_text
    print "Closest", idx
    print "Equation", eqn
    print "Answer", ans
    # assert idx == 6
    assert is_correct(ans, [[-1.0/3]])

    q_text = "The sum of two numbers is twenty two. The sum of the squares of the two numbers is two hundred and forty four. Find the two numbers."
    ans, eqn, idx = model.predict(q_text)
    print "\nQ:", q_text
    print "Closest", idx
    print "Equation", eqn
    print "Answer", ans
    # assert idx == 8
    assert is_correct(ans, [[10, 12]])

    print "... passed."

    # Check evaluation
    print "\nChecking evaluation ..."
    acc, results = model.evaluate(json_data[:20])
    print "accuracy:", acc

    for i, (x, y) in enumerate(zip(json_data[:20], results)):
        soln, eqns, idx, ans, correct = y

        if not correct:
            print "\nQ:", x[u"text"]
            print "EQNS*:", x[u"equations"]
            print "ANS*:", x[u"ans"]

            print "EQNS", eqns
            print "SOLN", soln
            print "ANS", ans
            print "IDX", idx
    print " ... end"

    # Test save and load
    print "\nChecking save and load ..."
    with open("test_model.pkl", "wb") as f:
        pickle.dump(model, f)

    del model
    with open("test_model.pkl", "rb") as f:
        model = pickle.load(f)

    acc, results = model.evaluate(json_data[:20])
    print "accuracy:", acc
    print " ... end"

def eval_sim_model():
    training_data = util.load_data("data/eval_cleaned.json")

    # Train and save model
    model = SimModel()
    # print "stop words", model.config.remove_stop_words
    # print "punct", model.config.remove_punct
    # print "tfidf", model.config.tfidf
    # print "tfidf norm", model.config.tfidf_norm

    model.fit(training_data)
    with open("sim_model.pkl", "wb") as f:
        pickle.dump(model, f)

    # Perform predictions
    test_data = util.load_data("data/dev_cleaned.json")[:1864]
    acc, results = model.evaluate(test_data)
    true_eqns = [x[u"equations"] for x in test_data]
    predicted_eqns = [x[1] for x in results]
    bleu_score = util.compute_bleu_score(true_eqns, predicted_eqns)
    print "Overall accuracy: {0:.4f}".format(acc)
    print "BLEU score: {0:.4f}".format(bleu_score)

    # Save results to file
    with open("eval_results.txt", "w") as f:
        f.write("Accuracy: {}\n".format(acc))
        f.write("BLEU: {}\n".format(bleu_score))
        for i, (x, r) in enumerate(zip(test_data, results)):
            soln, eqns, idx, ans, correct = r
            f.write(util.remove_non_printable("Q:" + x[u"text"] + "\n"))
            f.write(util.remove_non_printable(x[u"equations"] + "\n"))
            f.write(util.remove_non_printable(x[u"ans"] + "\n"))
            f.write(util.remove_non_printable("EQNS: {}\n".format(eqns)))
            f.write(util.remove_non_printable("SOLN: {}\n".format(soln)))
            f.write(util.remove_non_printable("ANS: {}\n".format(ans)))
            f.write(util.remove_non_printable("IDX: {}\n".format(idx)))
            f.write(util.remove_non_printable("CORRECT: {}\n".format(correct)))

def eval_saved_sim_model():
    test_data = util.load_data("data/dev_cleaned.json")[1864:]
    model = None
    with open("sim_model.pkl", "rb") as f:
        model = pickle.load(f)

    print "stop words", model.config.remove_stop_words
    print "punct", model.config.remove_punct
    print "tfidf", model.config.tfidf
    print "tfidf norm", model.config.tfidf_norm

    acc, results = model.evaluate(test_data)

    true_eqns = [x[u"equations"] for x in test_data]
    predicted_eqns = [x[1] for x in results]
    bleu_score = util.compute_bleu_score(true_eqns, predicted_eqns)
    print "Overall accuracy: {0:.4f}".format(acc)
    print "BLEU score: {0:.4f}".format(bleu_score)

if __name__ == "__main__":
    # eval_sim_model()
    # eval_saved_sim_model()
    check_glove_vectors()
