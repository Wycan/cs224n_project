import nltk
import glove
import util
from words2num import words2num
import numbers
import string

def is_not_number(x):
    return not isinstance(util.token2num(x), numbers.Number)

data = util.load_data("data/eval_cleaned.json")
vocab = set()

for x in data:
    text = x[u"text"].lower()
    tokens = nltk.word_tokenize(text)
    # tokens = filter(is_not_number, tokens)
    vocab |= set(tokens)
tokens_dict = dict(zip(vocab, range(len(vocab))))
print "Tokenization done."

i = 0
for k,v in tokens_dict.iteritems():
    if i == 10: break
    print k, v
    i += 1

tokens_dict = dict(zip(string.punctuation, range(len(string.punctuation))))
wordVectors = glove.loadWordVectors(tokens_dict)

print string.punctuation
