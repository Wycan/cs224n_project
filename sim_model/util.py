"""
General utility function
"""

import json
#import regex
import re
import string
from sympy import simplify
from words2num import words2num
import numbers
import nltk
from nltk.translate.bleu_score import corpus_bleu
import signal
from contextlib import contextmanager


class CONSTS(object):
    sub_dict = {u"twice": 2, u"thrice": 3, u"half": 0.5, u"third": 1.0/3,
                u"quarter": 1.0/4, u"fifth": 1.0/5}

# Load data from JSON
def load_data(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data

# Remove punctuation from unicode data
def remove_punctuation(text):
    return re.sub(ur"[^a-zA-Z ]+", "", text)

# Remove non numbers from unicode data
def remove_non_numbers(text):
    return re.sub(ur"[^0-9.\/\-e\^*;]+", "", text)

def replace_exp(text):
    return re.sub(ur"[\^]", "**", text)

def remove_brackets(text):
    return re.sub(ur"[{}]+", "", text)

def remove_units(text):
    return re.sub(ur" [a-zA-Z]+", "", text)

def remove_non_printable(text):
    printable = set(string.printable)
    return filter(lambda x: x in printable, text)

def insert_mult(text):
    # Brackets
    text = re.sub(ur"([^ +-\/*\^=\(])(\()",ur"\1*\2", text)
    text = re.sub(ur"(\))([^ +-\/*\^=\)])",ur"\1*\2", text)

    # Variables
    text = re.sub(ur"(\d)([a-df-z])", ur"\1*\2", text)
    text = re.sub(ur"([a-df-z])(\d)", ur"\1*\2", text)
    text = re.sub(ur"([a-z])([a-z])", ur"\1*\2", text)

    return text

def eqn_tokenizer(eqn):
    """ Tokenizes a single equation.
    Refer to https://stackoverflow.com/questions/20805614/tokenize-a-mathematical-expression-in-python
    Args:
        eqn: String containing a single equation.
    Returns:
        tokens: list containing tokens (strings) of equation.
    """
    if not eqn: return u""
    tokens = re.findall(r"(\b\w*[\.]?\w*[e\-]*\w+\b|[\(\)\+\*\-\/\^=<>])", eqn)
    return tokens

# Parse answer text
def parse_answer(text):
    ans = []
    for soln in text.split("or"):
        x = soln.split("|")[0]
        x = remove_non_numbers(x)
        x = replace_exp(x)
        ans.append([simplify(i).evalf() for i in x.split(";") if i])
    return ans

# Parse question text
def parse_equation(text):
    unkns_eqns = text.split("\r\n")
    unkns, eqns = unkns_eqns[0], unkns_eqns[1:]

    # Remove headers
    unkns = unkns.split(":")[1].split(",")
    eqns = [eqn.split(":")[1] for eqn in eqns]

    # Convert to equations
    parsed_eqns = []
    for eqn in eqns:
        eqn = insert_mult(eqn)
        lhs, rhs = re.split("=|<|>", eqn)
        eqn = lhs + "-(" + rhs + ")"
        parsed_eqns.append(eqn)

    return unkns, parsed_eqns

# Perform multiple string substitutions
def multiple_replace(text, sub_dict):
    rx = re.compile('|'.join(map(re.escape, sub_dict)))
    def one_xlat(match):
        return sub_dict[match.group(0)]
    return rx.sub(one_xlat, text)

def token2num(token):
    """ Converts the token to a number (if possible). Otherwise returns
    original token.
    """
    try:
        return float(token)
    except:
        pass
    try:
        return eval("1.0*"+token)
    except:
        pass
    try:
        return words2num(token)
    except:
        pass
    return token

def is_number(tokens):
    """ Determines if @tokens is a number or not.
    Args:
        tokens: list of words (strings)
    Returns:
        flag: True if number; false otherwise
    """
    x = " ".join(tokens)
    flag = isinstance(token2num(x), numbers.Number)
    return flag

def extract_nums(q_text):
    """ Extracts the significant numbers from the question text (@q_tokens).
    Args:
        q_text: question text (string).
    Returns:
        sig_nums: list of numbers (floats) that are significant.
    """
    q_text = q_text.lower()
    q_text = re.sub(ur"one(\s\w*){0,3}number|(two|three)(\s\w*){0,3}numbers", "", q_text)
    q_tokens = nltk.word_tokenize(q_text)

    # Concatenate consecutive number tokens
    i, tokens = 0, []
    while i < len(q_tokens):
        j = len(q_tokens)
        while j > i+1 and not is_number(q_tokens[i:j]):
            j -= 1
        tokens.append(token2num(" ".join(q_tokens[i:j])))
        i = j

    # Perform simple word replacements
    tokens = [CONSTS.sub_dict[x] if x in CONSTS.sub_dict
              else x for x in tokens]

    # Extract significant numbers
    sig_nums = filter(lambda x: isinstance(x, numbers.Number), tokens)
    return sig_nums

def extract_placeholders(args, eqns):
    """ Returns a list of strings from @eqn_text corresponding to each number
    in @args.
    Args:
        args: list of significant numbers from question text (float/int).
        eqns: list of parsed equations (string for each equation).
    Returns:
        placeholders: list of strings coresponding to each number in @nums.
    """
    eqn_tokens = [x for eqn in eqns for x in eqn_tokenizer(eqn)]
    num2placeholder = {x: "" for x in args}
    for x in eqn_tokens:
        for y in args:
            try:
                if not num2placeholder[y] and float(x) == y:
                    num2placeholder[y] = x
            except:
                pass
    placeholders = [num2placeholder[x] for x in args]
    return placeholders

class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def compute_bleu_score(true_eqns, predicted_eqns):
    """ Computes the corpus-level bleu score for @predicted_eqns vs.
    gold @true_eqns.
    Args:
        true_eqns: [x1[u"equations], x2[u"equations], ..., xN[u"equations"]]
        predicted_eqns: [[eq1, eq2], [eq1], ..., [eq1, eq2, eq3]]
    Returns:
        bleu_score: corpus-level bleu score across all examples
    """
    list_of_references, hypotheses = [], []
    for x, x_hat in zip(true_eqns, predicted_eqns):
        if not x: continue
        try:
            _, x_eqns = parse_equation(x)
        except:
            continue

        if not x_eqns: continue
        if not x_hat: continue
        x_tokens = reduce(lambda x, y: x + y, [eqn_tokenizer(i) for i in x_eqns])
        list_of_references.append([x_tokens])

        if not x_hat:
            x_hat_tokens = []
        else:
            x_hat_tokens = reduce(lambda x, y: x + y,
                                  [eqn_tokenizer(i) for i in x_hat])
        hypotheses.append(x_hat_tokens)

    # for x, y in zip(list_of_references, hypotheses):
    #     print ""
    #     print x
    #     print y

    bleu_score = corpus_bleu(list_of_references, hypotheses)
    return bleu_score

def test_load_data():
    print "\nChecking load data"
    data = load_data("data/dev_cleaned.json")
    print "Passed\n"

def test_remove_punctuation():
    print "\nChecking punctuation"
    x = u"Hello! I'm here"
    y = remove_punctuation(x)
    print x
    print y
    assert y == "Hello Im here"
    print "Passed\n"

    x = unicode(string.printable, "utf-8")
    y = remove_punctuation(x)
    print x
    print y
    assert y == unicode(string.ascii_letters+" ", "utf-8")
    print "Passed\n"

def test_remove_non_numbers():
    print "\nChecking removing non numbers"
    x = u"6; 9"
    y = remove_non_numbers(x)
    print x
    print y
    assert y == u"6;9"

    x = u"{6; 9; 6/2 or -4.932}"
    y = remove_non_numbers(x)
    print x
    print y
    assert y == u"6;9;6/2-4.932"

    print "Passed\n"

def test_multiple_replace():
    print "\nChecking multiple replace ..."
    x = u"hello there hello there I am well"
    sub_dict = {u"hello": u"good", u"there": u"bye"}
    y = multiple_replace(x, sub_dict)

    print x
    print y
    assert y == u"good bye good bye I am well"
    print "... passed."

def test_parse_answer():
    print "\nChecking parsing of answers ..."
    data = load_data("data/dev_cleaned.json")
    #SKIP_Q = {310}

    #print "Dataset"
    #for i, q in enumerate(data):
    #    if not q[u"equations"] or i in SKIP_Q: continue
    #    #print i, ")"
    #    #print q[u"ans"]
    #    parse_answer(q[u"ans"])
    #    #print ""

    # print "... passed."

    x = u"{3; 4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3, 4]]

    x = u"{0.33 or 4.0}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.33], [4.0]]

    x = u"{3 | 4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3]]

    x = u"{3/4 | 0.75}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75]]

    x = u"{0.75 | 3/4}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75]]

    x = u"{3; 4 | 3.0; 4.0}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3, 4]]

    x = u"3.3e-07; 2^8"
    y = parse_answer(x)
    print x
    print y
    assert y == [[3.3e-07, 2**8]]

    x = u"{0.75 | 3/4 or 4; 5; 3}"
    y = parse_answer(x)
    print x
    print y
    assert y == [[0.75], [4, 5, 3]]

    print "... passed."

def test_parse_equation():
    data = load_data("data/dev_cleaned.json")
    print "\nChecking parsing equation ..."

    print "1) Checking data ..."
    for q in data[:10]:
        x = q[u"equations"]
        if not x or u">" in x or u"<" in x: continue
        y = parse_equation(x)
        print x
        print y, "\n"
    print "... passed."

    print "2) Test cases ..."
    x = u"unkn: x,y\r\nequ: 2(2*x) = y\r\nequ: (1+y)(2+y)=10"
    print x
    unkns, eqns = parse_equation(x)
    print eqns, "\n"
    assert eqns[0] == u" 2*(2*x) -( y)"
    assert eqns[1] == u" (1+y)*(2+y)-(10)"
    print "... passed."

def test_insert_mult():
    print "\nChecking insert_mult ..."

    x = u"(99+3)n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*n"

    x = u"(99+3)*n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*n"

    x = u"(99+3)+n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)+n"

    x = u"(99+3)-n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)-n"

    x = u"(99+3)/n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)/n"

    x = u"(99+3)^n"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)^n"

    x = u"(99+3)(100 + 2)"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3)*(100 + 2)"

    x = u"(99+3) + (3 + 2)"
    y = insert_mult(x)
    print x, y
    assert y == u"(99+3) + (3 + 2)"

    x = u"n(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)"

    x = u"n*(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)"

    x = u"n+(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n+(99+3)"

    x = u"n-(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n-(99+3)"

    x = u"n/(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n/(99+3)"

    x = u"n^(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n^(99+3)"

    x = u"n(99+3)n(99+3)"
    y = insert_mult(x)
    print x, y
    assert y == u"n*(99+3)*n*(99+3)"

    x = u"5n(4 + 1) + xy + 7e-7"
    y = insert_mult(x)
    print x, y
    assert y == u"5*n*(4 + 1) + x*y + 7e-7"

    print "... passed."

def test_eqn_tokenizer():
    print "\nChecking equation tokenizer ..."
    x = u"5*n + 4 = 7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"5", u"*", u"n", u"+", u"4", u"=", u"7"]

    x = u"5.44*n + 4 = 7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"5.44", u"*", u"n", u"+", u"4", u"=", u"7"]

    x = u"sin(n) ^ 4e06 = -7"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"sin", u"(", u"n", u")", u"^", u"4e06", u"=", u"-", u"7"]

    x = u"4.2e-06 < -7e-07 < 2*(log(x))"
    y = eqn_tokenizer(x)
    print x
    print y
    assert y == [u"4.2e-06", u"<", u"-", u"7e-07", u"<", u"2", u"*", u"(", u"log", u"(", u"x", u")", u")"]

    print "... passed."

def test_is_number():
    print "\nChecking is_number ..."
    x = [u"Twenty"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"Twenty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"One", u"hundred", u"and", u"thirty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == True

    x = [u"Not", u"hundred", u"and", u"thirty", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == False

    x = [u"Not", u"three"]
    flag = is_number(x)
    print x, flag
    assert flag == False

    x = [u"three", u"Not"]
    flag = is_number(x)
    print x, flag
    assert flag == False

def test_extract_nums():
    print "\nChecking number extraction ..."
    q_text = u"One number is 3 less than a second number. Twice the second number is 12 less than 5 times the first. Find the two numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [3, 2, 12, 5]

    q_text = u"The sum of two numbers is 125. Five times one number minus three times the other is 41. Find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [125, 5, 3, 41]

    q_text = u"1/3 of a number is 5 less than half of the same number. what is the number?"
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [1.0/3, 5, 1.0/2]

    q_text = u"Eighty, decreased by three times a number, is the same as five times the number, increased by eight. Find the number."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [80, 3, 5, 8]

    q_text = u"the product of two consecutive odd numbers is 143. find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [143]

    q_text = u"the product of two consecutive odd numbers is twenty three. find the numbers."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [23]

    q_text = u"One hundred and eighty three, decreased by three times a number, is the same as five times the number, increased by eight. Find the number."
    sig_nums = extract_nums(q_text)
    print q_text
    print sig_nums
    assert sig_nums == [183, 3, 5, 8]

    print "... passed."

def test_time_limit():
    import time

    print "\nChecking time_limit ..."
    try:
        with time_limit(5):
            time.sleep(5.1)
    except TimeoutException as e:
        print("Timed out!")

    try:
        with time_limit(5):
            time.sleep(4.9)
    except TimeoutException as e:
        print("Timed out!")

    print "... passed"

def test_compute_bleu_score():
    print "\nChecking compute bleu score ..."
    data = load_data("data/eval_cleaned.json")[:10]
    true_eqns = [x[u"equations"] for x in data]
    predicted_eqns = [parse_equation(x)[1] for x in true_eqns]
    y = compute_bleu_score(true_eqns, predicted_eqns)
    assert abs(y - 1.00) < 1e-3
    print "bleu score: {0: .4f}".format(y)
    print "... passed"

if __name__ == "__main__":
    test_insert_mult()
    test_parse_equation()
    test_load_data()
    test_remove_punctuation()
    test_multiple_replace()
    test_remove_non_numbers()
    test_parse_answer()
    test_eqn_tokenizer()
    test_is_number()
    test_extract_nums()
    test_time_limit()
    test_compute_bleu_score()
