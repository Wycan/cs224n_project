import util
from equation import Equation


class Example(object):
    def __init__(json_obj):
        self.question = json_obj[u"text"]
        self.equation = Equation()
        self.answer = util.parse_answer()
