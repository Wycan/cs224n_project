from sympy import Symbol, symbols, solve, simplify, Eq, lambdify, sin, Float, Integer, Number
from sympy.parsing.sympy_parser import parse_expr
from sympy.parsing.sympy_parser import standard_transformations, implicit_multiplication_application
from sympy.parsing.sympy_tokenize import generate_tokens

from sympy.solvers.inequalities import solve_rational_inequalities
import numpy as np
import util
from equation import Equation

# print solve(["x + 5 - y", "3*y + 15 - 2*x"])
#
# data = util.load_data("data/eval_cleaned.json")
# print len(data)
#
# data = util.load_data("data/dev_cleaned.json")
# print len(data)
# x = u""
#
# if not x:
#     print "hello"
transformations = (standard_transformations + (implicit_multiplication_application,))

eqn = parse_expr("2x(1-x) * 2 + 2", transformations=transformations)
print eqn
print eqn.args

# Atoms
args = {x: 4 for x in eqn.atoms(Number)}
print eqn.subs(args)

# for x in generate_tokens("2x(1-x) > 2"):
#     print x
# print tokens


# print "hello {0: .2f}".format(0.2222)

# x = np.zeros(10)
# np.add.at(x, np.array([], dtype=np.int), 1)
# print x


#
# x = data[103]
#
# eqns = Equation(x)
#
# print x
# print eqns.eqns
# print eqns.unkns
# print eqns.default_args
# print eqns.default_args
# print eqns.eval()

# ans = util.parse_answer(x[u"ans"])
# soln, _ = eqns.eval()

# with open("test.txt", "w") as f:
#     for i in xrange(10):
#         f.write("{}) Here".format(i))
