"""
Helper functions to download files from Google Colaboratory.
"""
from google.colab import files
import zipfile
import os

def checkpoint_download(checkpoint_dir, checkpoint_prefix, log_dir, events_dir):
    zip_filename = checkpoint_prefix + ".zip"
    with zipfile.ZipFile(zip_filename, 'w') as ziph:
        # Save hyperparameters
        ziph.write(os.path.join(checkpoint_dir, "hparams"))

        # Save checkpoint
        ziph.write(checkpoint_prefix + ".index")
        ziph.write(checkpoint_prefix + ".meta")
        ziph.write(checkpoint_prefix + ".data-00000-of-00001")
        ziph.write(os.path.join(checkpoint_dir, "checkpoint"))

        # Save log (log of training progress)
        log_file = [f for f in os.listdir(log_dir) if filename.startswith("log")][0]
        ziph.write(log_file)

        # Save events file (for TensorBoard)
        events_file = [f for f in os.listdir(events_dir) if filename.startswith("events")][0]
        ziph.write(events_file)

    files.download(zip_filename)

def zipdir(zip_filename, dir_path):
    """ Creates a zip file called @zip_filename containing all the files
    in @dir_path (includes all files within sub-directories as well)."""
    with zipfile.ZipFile(zip_filename, 'w') as ziph:
        for root, dirs, files in os.walk(dir_path):
            for f in files:
                ziph.write(os.path.join(root, f))

# def test():
#     for f in os.listdir("."):
#         if "test" in f:
#             print f

if __name__ == "__main__":
    # test()
    pass
