#!/bin/bash
mkdir char_model_256
python -m nmt.nmt \
--src=text \
--tgt=eqns \
--vocab_prefix=../data/vocab_chars \
--embed_prefix=../data/glove.6B.100d \
--share_vocab=true \
--decoder_vocab_size=41 \
--train_prefix=../data/train/train_chars \
--dev_prefix=../data/dev/dev_chars \
--test_prefix=../data/test/test_chars \
--out_dir=char_model_256 \
--num_train_steps=12000 \
--steps_per_stats=100 \
--encoder_type="bi" \
--num_layers=2 \
--num_units=256 \
--optimizer=adam \
--learning_rate=0.001 \
--batch_size=64 \
--attention=scaled_luong \
--dropout=0.2 \
--metrics=bleu

mkdir char_model_512
python -m nmt.nmt \
--src=text \
--tgt=eqns \
--vocab_prefix=../data/vocab_chars \
--embed_prefix=../data/glove.6B.100d \
--share_vocab=true \
--decoder_vocab_size=41 \
--train_prefix=../data/train/train_chars \
--dev_prefix=../data/dev/dev_chars \
--test_prefix=../data/test/test_chars \
--out_dir=char_model_512 \
--num_train_steps=12000 \
--steps_per_stats=100 \
--encoder_type="bi" \
--num_layers=2 \
--num_units=512 \
--optimizer=adam \
--learning_rate=0.001 \
--batch_size=64 \
--attention=scaled_luong \
--dropout=0.2 \
--metrics=bleu

mkdir char_model_1024
python -m nmt.nmt \
--src=text \
--tgt=eqns \
--vocab_prefix=../data/vocab_chars \
--embed_prefix=../data/glove.6B.100d \
--share_vocab=true \
--decoder_vocab_size=41 \
--train_prefix=../data/train/train_chars \
--dev_prefix=../data/dev/dev_chars \
--test_prefix=../data/test/test_chars \
--out_dir=char_model_1024 \
--num_train_steps=12000 \
--steps_per_stats=100 \
--encoder_type="bi" \
--num_layers=2 \
--num_units=256 \
--optimizer=adam \
--learning_rate=0.001 \
--batch_size=64 \
--attention=scaled_luong \
--dropout=0.2 \
--metrics=bleu
